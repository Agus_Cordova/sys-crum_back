# SysCRUM-back


Repositorio para el backend de la aplicación SysCRUM, construido con arquetipo Quarkus e integrando el core de [SESVER](https://gitlab.com/sesver-backend/sesver-core/-/tree/quarkus-test).

## Built With

* 	[Maven](https://maven.apache.org/) - Dependency Management
* 	[JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit
* 	[Quarkus](https://quarkus.io/) - A Kubernetes Native Java stack tailored for OpenJDK HotSpot and GraalVM, crafted from the best of breed Java libraries and standards. 
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system
* 	[OpenAPI](https://swagger.io/specification/) - Open-Source software framework backed by a large ecosystem of tools that helps developers design, build, document, and consume RESTful Web services.
* 	[JUnit](https://junit.org/) - Testing framework for Java
* 	[PostgreSQL](https://www.postgresql.org/) - Open source object-relational database 


## External Tools Used

* 	[Postman](https://www.getpostman.com/) - API Development Environment (Testing Docmentation)

## To-Do

* 	[x] Implementar clases de core en Resources de la app
* 	[x] Configurar la BD
* 	[x] Configurar OpenApi


## Running the application locally (dev)


La aplicación se corre desde consola con Maven utilizando el plugin de Quarkus y el goal dev: 

```shell
mvn quarkus:dev
```
##When the service is running

openAPI definition  (pending)
* Please go to http://localhost:8080/api

### Web Page URLs

Pending 